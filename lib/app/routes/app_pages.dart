import 'package:get/get.dart';

import 'package:elemes_app/app/modules/detail/bindings/detail_binding.dart';
import 'package:elemes_app/app/modules/detail/views/detail_view.dart';
import 'package:elemes_app/app/modules/detail_tv/bindings/detail_tv_binding.dart';
import 'package:elemes_app/app/modules/detail_tv/views/detail_tv_view.dart';
import 'package:elemes_app/app/modules/home/bindings/home_binding.dart';
import 'package:elemes_app/app/modules/home/views/home_view.dart';
import 'package:elemes_app/app/modules/now_playing/bindings/now_playing_binding.dart';
import 'package:elemes_app/app/modules/now_playing/views/now_playing_view.dart';
import 'package:elemes_app/app/modules/on_air_tv/bindings/on_air_tv_binding.dart';
import 'package:elemes_app/app/modules/on_air_tv/views/on_air_tv_view.dart';
import 'package:elemes_app/app/modules/on_today_tv/bindings/on_today_tv_binding.dart';
import 'package:elemes_app/app/modules/on_today_tv/views/on_today_tv_view.dart';
import 'package:elemes_app/app/modules/people/bindings/people_binding.dart';
import 'package:elemes_app/app/modules/people/views/people_view.dart';
import 'package:elemes_app/app/modules/popular/bindings/popular_binding.dart';
import 'package:elemes_app/app/modules/popular/views/popular_view.dart';
import 'package:elemes_app/app/modules/popular_people/bindings/popular_people_binding.dart';
import 'package:elemes_app/app/modules/popular_people/views/popular_people_view.dart';
import 'package:elemes_app/app/modules/popular_tv/bindings/popular_tv_binding.dart';
import 'package:elemes_app/app/modules/popular_tv/views/popular_tv_view.dart';
import 'package:elemes_app/app/modules/session/bindings/session_binding.dart';
import 'package:elemes_app/app/modules/session/views/session_view.dart';
import 'package:elemes_app/app/modules/splash/bindings/splash_binding.dart';
import 'package:elemes_app/app/modules/splash/views/splash_view.dart';
import 'package:elemes_app/app/modules/top_rated/bindings/top_rated_binding.dart';
import 'package:elemes_app/app/modules/top_rated/views/top_rated_view.dart';
import 'package:elemes_app/app/modules/top_rated_tv/bindings/top_rated_tv_binding.dart';
import 'package:elemes_app/app/modules/top_rated_tv/views/top_rated_tv_view.dart';
import 'package:elemes_app/app/modules/upcoming/bindings/upcoming_binding.dart';
import 'package:elemes_app/app/modules/upcoming/views/upcoming_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.SPLASH;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.NOW_PLAYING,
      page: () => NowPlayingView(),
      binding: NowPlayingBinding(),
    ),
    GetPage(
      name: _Paths.TOP_RATED,
      page: () => TopRatedView(),
      binding: TopRatedBinding(),
    ),
    GetPage(
      name: _Paths.UPCOMING,
      page: () => UpcomingView(),
      binding: UpcomingBinding(),
    ),
    GetPage(
      name: _Paths.SPLASH,
      page: () => SplashView(),
      binding: SplashBinding(),
    ),
    GetPage(
      name: _Paths.POPULAR,
      page: () => PopularView(),
      binding: PopularBinding(),
    ),
    GetPage(
      name: _Paths.POPULAR_TV,
      page: () => PopularTvView(),
      binding: PopularTvBinding(),
    ),
    GetPage(
      name: _Paths.TOP_RATED_TV,
      page: () => TopRatedTvView(),
      binding: TopRatedTvBinding(),
    ),
    GetPage(
      name: _Paths.ON_AIR_TV,
      page: () => OnAirTvView(),
      binding: OnAirTvBinding(),
    ),
    GetPage(
      name: _Paths.ON_TODAY_TV,
      page: () => OnTodayTvView(),
      binding: OnTodayTvBinding(),
    ),
    GetPage(
      name: _Paths.POPULAR_PEOPLE,
      page: () => PopularPeopleView(),
      binding: PopularPeopleBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL,
      page: () => DetailView(),
      binding: DetailBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_TV,
      page: () => DetailTvView(),
      binding: DetailTvBinding(),
    ),
    GetPage(
      name: _Paths.SESSION,
      page: () => SessionView(),
      binding: SessionBinding(),
    ),
    GetPage(
      name: _Paths.PEOPLE,
      page: () => PeopleView(),
      binding: PeopleBinding(),
    ),
  ];
}
