part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();

  static const HOME = _Paths.HOME;
  static const NOW_PLAYING = _Paths.NOW_PLAYING;
  static const TOP_RATED = _Paths.TOP_RATED;
  static const UPCOMING = _Paths.UPCOMING;
  static const SPLASH = _Paths.SPLASH;
  static const POPULAR = _Paths.POPULAR;
  static const POPULAR_TV = _Paths.POPULAR_TV;
  static const TOP_RATED_TV = _Paths.TOP_RATED_TV;
  static const ON_AIR_TV = _Paths.ON_AIR_TV;
  static const ON_TODAY_TV = _Paths.ON_TODAY_TV;
  static const POPULAR_PEOPLE = _Paths.POPULAR_PEOPLE;
  static const DETAIL = _Paths.DETAIL;
  static const DETAIL_TV = _Paths.DETAIL_TV;
  static const SESSION = _Paths.SESSION;
  static const PEOPLE = _Paths.PEOPLE;
}

abstract class _Paths {
  static const HOME = '/home';
  static const NOW_PLAYING = '/now-playing';
  static const TOP_RATED = '/top-rated';
  static const UPCOMING = '/upcoming';
  static const SPLASH = '/splash';
  static const POPULAR = '/popular';
  static const POPULAR_TV = '/popular-tv';
  static const TOP_RATED_TV = '/top-rated-tv';
  static const ON_AIR_TV = '/on-air-tv';
  static const ON_TODAY_TV = '/on-today-tv';
  static const POPULAR_PEOPLE = '/popular-people';
  static const DETAIL = '/detail';
  static const DETAIL_TV = '/detail-tv';
  static const SESSION = '/session';
  static const PEOPLE = '/people';
}
