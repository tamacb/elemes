import 'package:get/get.dart';

import '../controllers/popular_people_controller.dart';

class PopularPeopleBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PopularPeopleController>(
      () => PopularPeopleController(),
    );
  }
}
