import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/popular_people_controller.dart';

class PopularPeopleView extends GetView<PopularPeopleController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('PopularPeopleView'),
        centerTitle: true,
      ),
      body: Center(
        child: Text(
          'PopularPeopleView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
