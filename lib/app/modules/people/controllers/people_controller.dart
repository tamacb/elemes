import 'package:elemes_app/app/model/people_model.dart';
import 'package:elemes_app/app/modules/people/providers/people_provider.dart';
import 'package:get/get.dart';

import '../../../utils/utils.dart';

class PeopleController extends GetxController {
  PeopleProvider peopleProvider;

  PeopleController({required this.peopleProvider});

  final page = 1.obs;
  late RxInt maxPage = 0.obs;

  final isLoadingPeople = false.obs;

  @override
  void onInit() {
    super.onInit();
    getPeople();
  }

  Future<int> incrementPage() async => page.value++;

  final resultPeople = <ResultPeople>[].obs;

  Future getPeople() async {
    try {
      isLoadingPeople.value = true;
      final res = await peopleProvider.getPeople(page: page.value.toString());
      // logger.wtf(res!.data!.data!.toList());
      resultPeople.assignAll(res!.results!);
      maxPage.value = res.totalPages!;
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingPeople.value = false;
    }
  }

  Future loadMorePeople() async {
    if (page.value != maxPage.value){
      await incrementPage();
      await getMorePeople();
    }
  }

  Future getMorePeople() async {
    try {
      isLoadingPeople.value = true;
      final res = await peopleProvider.getPeople(page: page.value.toString());
      // logger.wtf(res!.data!.data!.toList());
      resultPeople.addAll(res!.results!);
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingPeople.value = false;
    }
  }
}
