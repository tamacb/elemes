import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';

import '../controllers/people_controller.dart';

class PeopleView extends GetView<PeopleController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Popular Casting',
          style: GoogleFonts.nunitoSans(
            color: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.w600,
          ),
        ),
        flexibleSpace: const Image(
          image: AssetImage('assets/bg.jpg'),
          fit: BoxFit.cover,
        ),
        centerTitle: true,
        leading: const SizedBox(height: 1),
      ),
      body: Obx(() => LazyLoadScrollView(
        onEndOfPage: () => controller.loadMorePeople(),
        isLoading: controller.isLoadingPeople.value,
        child: ListView(
          children: [
            AlignedGridView.count(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              crossAxisCount: 3,
              mainAxisSpacing: 4,
              crossAxisSpacing: 4,
              itemCount: controller.resultPeople.length,
              itemBuilder: (context, index) => Card(
                  child: GestureDetector(
                    onTap: () {
                      // _detailController
                      //     .getDetail(id: controller.resultPopularMovie[index].id.toString())
                      //     .whenComplete(() => Get.to(DetailView()));
                    },
                    child: Container(
                      height: Get.height * 0.28,
                      color: Colors.white,
                      child: Column(
                        children: [
                          (controller.resultPeople[index].profilePath == null)
                              ? const Text('Image not available')
                              : ClipRRect(
                            borderRadius: BorderRadius.circular(5.0),
                            child: SizedBox(
                              height: Get.height * 0.22,
                              child: Image.network(
                                'https://image.tmdb.org/t/p/w500/${controller.resultPeople[index].profilePath}',
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 4.0),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                controller.resultPeople[index].name!,
                                style: GoogleFonts.openSans(
                                  color: Colors.black,
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold,
                                ),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 4.0),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text('Popularity ${controller.resultPeople[index].popularity}',
                                style: GoogleFonts.openSans(
                                  color: Colors.black,
                                  fontSize: 12,
                                  fontWeight: FontWeight.normal,
                                ),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  )),
            ),
            if (controller.isLoadingPeople.value)
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: Center(
                    child: CircularProgressIndicator(
                      semanticsLabel: 'loading data',
                    )),
              )
          ],
        ),
      )),
    );
  }
}
