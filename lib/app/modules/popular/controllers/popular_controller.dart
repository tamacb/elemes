import 'package:elemes_app/app/model/popular_movie_model.dart';
import 'package:get/get.dart';

import '../../../utils/utils.dart';
import '../providers/popular_movie_provider.dart';

class PopularController extends GetxController {
  PopularMovieProvider popularMovieProvider;

  PopularController({required this.popularMovieProvider});

  final page = 1.obs;
  late RxInt maxPage = 0.obs;

  final isLoadingPopularMovie = false.obs;

  @override
  void onInit() {
    super.onInit();
    getAllPopular();
  }

  Future<int> incrementPage() async => page.value++;

  final resultPopularMovie = <ResultPopularMovie>[].obs;

  Future getAllPopular() async {
    try {
      isLoadingPopularMovie.value = true;
      final res = await popularMovieProvider.getPopularMovie(page: page.value.toString());
      // logger.wtf(res!.data!.data!.toList());
      resultPopularMovie.assignAll(res!.results!);
      maxPage.value = res.totalPages!;
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingPopularMovie.value = false;
    }
  }

  Future loadMorePopularMovie() async {
    if (page.value != maxPage.value){
      await incrementPage();
      await getMorePopularMovie();
    }
  }

  Future getMorePopularMovie() async {
    try {
      isLoadingPopularMovie.value = true;
      final res = await popularMovieProvider.getPopularMovie(page: page.value.toString());
      // logger.wtf(res!.data!.data!.toList());
      resultPopularMovie.addAll(res!.results!);
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingPopularMovie.value = false;
    }
  }
}
