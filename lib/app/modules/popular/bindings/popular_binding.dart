import 'package:elemes_app/app/modules/popular/providers/popular_movie_provider.dart';
import 'package:get/get.dart';

import '../controllers/popular_controller.dart';

class PopularBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PopularController>(
      () => PopularController(popularMovieProvider: PopularMovieProvider()),
    );
  }
}
