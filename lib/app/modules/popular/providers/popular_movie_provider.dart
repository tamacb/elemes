import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:elemes_app/app/model/popular_movie_model.dart';
import 'package:flutter_flavor/flutter_flavor.dart';
import 'package:get/get.dart';

import '../../../data/http_service.dart';
import '../../../utils/utils.dart';

class PopularMovieProvider extends GetConnect {
  Future<PopularMovieModel?> getPopularMovie({String? page}) async {
    var baseUrl = FlavorConfig.instance.variables["baseUrl"];
    Uri _getPopularMovie = Uri.parse(baseUrl).replace(
        queryParameters: {"api_key": HttpService.apiKey, "language": "en-US", "page": '$page'},
        pathSegments: ['3', 'movie', 'popular']);
    logger.wtf('ini adalah baseUrl $_getPopularMovie');
    final response = await http.get(_getPopularMovie, headers: HttpService.headers);
    if (response.statusCode == 200) {
      logger.wtf(response.statusCode);
      var jsonString = response.body;
      logger.wtf(jsonDecode(jsonString));
      return popularMovieModelFromJson(jsonString);
    }
    return null;
  }
}
