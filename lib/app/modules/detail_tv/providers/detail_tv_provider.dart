import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:elemes_app/app/model/detail_tv_model.dart';
import 'package:flutter_flavor/flutter_flavor.dart';
import 'package:get/get.dart';

import '../../../data/http_service.dart';
import '../../../model/common_message_model.dart';
import '../../../utils/utils.dart';

class DetailTvProvider extends GetConnect {
  Future<DetailTvModel?> getDetailTv({String? id}) async {
    var baseUrl = FlavorConfig.instance.variables["baseUrl"];
    Uri _getDetail = Uri.parse(baseUrl)
        .replace(queryParameters: {"api_key": HttpService.apiKey, "language": "en-US"}, pathSegments: ['3', 'tv', '$id']);
    logger.wtf('ini adalah baseUrl $_getDetail');
    final response = await http.get(_getDetail, headers: HttpService.headers);
    if (response.statusCode == 200) {
      logger.wtf(response.statusCode);
      var jsonString = response.body;
      logger.wtf(jsonDecode(jsonString));
      return detailTvModelFromJson(jsonString);
    }
    return null;
  }

  Future<CommonMessageModel?> rateTv({required Map<String, dynamic> body, String? idMovie, String? sessionId}) async {
    var baseUrl = FlavorConfig.instance.variables["baseUrl"];
    Uri _giveRateUrl = Uri.parse(baseUrl).replace(
        queryParameters: {"api_key": HttpService.apiKey, "language": "en-US", "guest_session_id": "$sessionId"},
        pathSegments: ['3', 'tv', '$idMovie', 'rating']);
    logger.wtf(_giveRateUrl);
    final response = await http.post(_giveRateUrl, headers: HttpService.headers, body: jsonEncode(body));
    logger.wtf(response.body);
    var jsonString = response.body;
    logger.wtf(jsonDecode(jsonString));
    return commonMessageModelFromJson(jsonString);
  }
}
