import 'package:elemes_app/app/model/detail_tv_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../../data/const.dart';
import '../../../model/common_message_model.dart';
import '../../../utils/utils.dart';
import '../../session/controllers/session_controller.dart';
import '../../session/providers/session_provider.dart';
import '../providers/detail_tv_provider.dart';

class DetailTvController extends GetxController {
  DetailTvProvider detailTvProvider;
  DetailTvController({required this.detailTvProvider});

  final SessionController _sessionController = Get.put(SessionController(sessionProvider: SessionProvider()));

  final count = 0.obs;
  final tvId = "".obs;
  final isLoadingDetailTv = false.obs;
  final isLoadingRateTv = false.obs;

  var boxSession = GetStorage();

  @override
  void onClose() {}
  void increment() => count.value++;

  final ratingEditingController = TextEditingController();

  final resultDetail = <DetailTvModel>[].obs;
  final resultSeason = <Season>[].obs;
  final resultNetworkMovie = <Network>[].obs;

  Future getDetailTv({String? id}) async {
    try {
      isLoadingDetailTv.value = true;
      final res = await detailTvProvider.getDetailTv(id: '$id');
      resultDetail.assign(res!);
      resultSeason.assignAll(res.seasons!);
      resultNetworkMovie.assignAll(res.networks!);
      tvId.value = res.id.toString();
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingDetailTv.value = false;
    }
  }

  final resultRateTv = <CommonMessageModel>[].obs;

  Future rateTv() async {
    try {
      isLoadingRateTv.value = true;
      final res = await detailTvProvider.rateTv(
          body: {"value": ratingEditingController.text}, idMovie: tvId.value, sessionId: boxSession.read(guestSessionId));
      if (res!.success == false) {
        Get.snackbar('${res.statusMessage}', 'Yapp invalid value or token expired, just try again', backgroundColor: Colors.white);
        _sessionController.getGuestSession();
      } else {
        Get.snackbar('${res.statusMessage}', 'your rating saved', backgroundColor: Colors.white);
      }
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingRateTv.value = false;
      ratingEditingController.clear();
    }
  }
}
