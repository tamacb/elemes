import 'package:elemes_app/app/modules/detail_tv/providers/detail_tv_provider.dart';
import 'package:get/get.dart';

import '../controllers/detail_tv_controller.dart';

class DetailTvBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailTvController>(
      () => DetailTvController(detailTvProvider: DetailTvProvider()),
    );
  }
}
