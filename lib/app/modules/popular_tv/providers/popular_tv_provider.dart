import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:elemes_app/app/model/tv_model.dart';
import 'package:flutter_flavor/flutter_flavor.dart';
import 'package:get/get.dart';

import '../../../data/http_service.dart';
import '../../../utils/utils.dart';

class PopularTvProvider extends GetConnect {
  Future<TvModel?> getPopularTv({String? page}) async {
    var baseUrl = FlavorConfig.instance.variables["baseUrl"];
    Uri _getPopularTv = Uri.parse(baseUrl).replace(
        queryParameters: {"api_key": HttpService.apiKey, "language": "en-US", "page": '$page'},
        pathSegments: ['3', 'tv', 'popular']);
    logger.wtf('ini adalah baseUrl $_getPopularTv');
    final response = await http.get(_getPopularTv, headers: HttpService.headers);
    if (response.statusCode == 200) {
      logger.wtf(response.statusCode);
      var jsonString = response.body;
      logger.wtf(jsonDecode(jsonString));
      return tvModelFromJson(jsonString);
    }
    return null;
  }
}
