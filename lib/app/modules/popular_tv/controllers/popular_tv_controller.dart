import 'package:elemes_app/app/model/tv_model.dart';
import 'package:elemes_app/app/modules/popular_tv/providers/popular_tv_provider.dart';
import 'package:get/get.dart';

import '../../../utils/utils.dart';

class PopularTvController extends GetxController {
  PopularTvProvider popularTvProvider;

  PopularTvController({required this.popularTvProvider});

  final page = 1.obs;
  late RxInt maxPage = 0.obs;

  final isLoadingPopularTv = false.obs;

  @override
  void onInit() {
    super.onInit();
    getAllPopularTv();
  }

  Future<int> incrementPage() async => page.value++;

  final resultPopularTv = <ResultTv>[].obs;

  Future getAllPopularTv() async {
    try {
      isLoadingPopularTv.value = true;
      final res = await popularTvProvider.getPopularTv(page: page.value.toString());
      // logger.wtf(res!.data!.data!.toList());
      resultPopularTv.assignAll(res!.results!);
      maxPage.value = res.totalPages!;
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingPopularTv.value = false;
    }
  }

  Future loadMorePopularTv() async {
    if (page.value != maxPage.value){
      await incrementPage();
      await getMorePopularTv();
    }
  }

  Future getMorePopularTv() async {
    try {
      isLoadingPopularTv.value = true;
      final res = await popularTvProvider.getPopularTv(page: page.value.toString());
      // logger.wtf(res!.data!.data!.toList());
      resultPopularTv.addAll(res!.results!);
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingPopularTv.value = false;
    }
  }
}
