import 'package:elemes_app/app/modules/popular_tv/providers/popular_tv_provider.dart';
import 'package:get/get.dart';

import '../controllers/popular_tv_controller.dart';

class PopularTvBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PopularTvController>(
      () => PopularTvController(popularTvProvider: PopularTvProvider()),
    );
  }
}
