import 'package:get/get.dart';

import '../../../model/now_playing_model.dart';
import '../../../utils/utils.dart';
import '../providers/now_playing_provider.dart';

class NowPlayingController extends GetxController {
  NowPlayingProvider nowPlayingProvider;

  NowPlayingController({required this.nowPlayingProvider});

  final isLoadingNowPlaying = false.obs;
  final page = 1.obs;
  @override
  void onInit() {
    super.onInit();
    getAllNowPlaying();
  }

  Future<int> incrementPage() async => page.value++;

  final resultNowPlaying = <ResultNowPlaying>[].obs;

  Future getAllNowPlaying() async {
    try {
      isLoadingNowPlaying.value = true;
      final res = await nowPlayingProvider.getNowPlaying(page: page.value.toString());
      // logger.wtf(res!.data!.data!.toList());
      resultNowPlaying.assignAll(res!.results!);
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingNowPlaying.value = false;
    }
  }

  Future getMoreNowPlaying() async {
    try {
      isLoadingNowPlaying.value = true;
      final res = await nowPlayingProvider.getNowPlaying(page: page.value.toString());
      // logger.wtf(res!.data!.data!.toList());
      resultNowPlaying.addAll(res!.results!);
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingNowPlaying.value = false;
    }
  }

  Future loadMoreNowPlaying() async {
    await incrementPage();
    await getMoreNowPlaying();
  }
}
