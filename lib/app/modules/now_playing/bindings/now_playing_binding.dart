import 'package:elemes_app/app/modules/now_playing/providers/now_playing_provider.dart';
import 'package:get/get.dart';

import '../controllers/now_playing_controller.dart';

class NowPlayingBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<NowPlayingController>(
      () => NowPlayingController(nowPlayingProvider: NowPlayingProvider()),
    );
  }
}
