import 'dart:convert';

import 'package:flutter_flavor/flutter_flavor.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

import '../../../data/http_service.dart';
import '../../../model/now_playing_model.dart';
import '../../../utils/utils.dart';

class NowPlayingProvider extends GetConnect {
  Future<NowPlayingModel?> getNowPlaying({String? page}) async {
    var baseUrl = FlavorConfig.instance.variables["baseUrl"];
    Uri _getNowPlaying = Uri.parse(baseUrl).replace(
        queryParameters: {"api_key": HttpService.apiKey, "language": "en-US", "page": "$page"},
        pathSegments: ['3', 'movie', 'now_playing']);
    logger.wtf('ini adalah baseUrl $_getNowPlaying');
    final response = await http.get(_getNowPlaying, headers: HttpService.headers);
    if (response.statusCode == 200) {
      logger.wtf(response.statusCode);
      var jsonString = response.body;
      logger.wtf(jsonDecode(jsonString));
      return nowPlayingModelFromJson(jsonString);
    }
    return null;
  }
}
