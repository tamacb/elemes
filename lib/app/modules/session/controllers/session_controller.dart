import 'package:elemes_app/app/modules/session/providers/session_provider.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../../data/const.dart';
import '../../../model/guest_session_model.dart';
import '../../../utils/utils.dart';

class SessionController extends GetxController {

  SessionProvider sessionProvider;

  SessionController({required this.sessionProvider});

  final boxSession = GetStorage();

  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  final isLoadingGuestSession = false.obs;

  final resultGuestSession = <GuestSessionModel>[].obs;

  Future getGuestSession() async {
    try {
      isLoadingGuestSession.value = true;
      final res = await sessionProvider.getGuestSession();
      // logger.wtf(res!.data!.data!.toList());
      if (res!.success == true) {
        resultGuestSession.assign(res);
        boxSession.write(guestSessionId, res.guestSessionId);
      }
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingGuestSession.value = false;
      logger.i(boxSession.read(guestSessionId));
    }
  }
}
