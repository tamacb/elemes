import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter_flavor/flutter_flavor.dart';
import 'package:get/get.dart';

import '../../../data/http_service.dart';
import '../../../model/guest_session_model.dart';
import '../../../utils/utils.dart';

class SessionProvider extends GetConnect {
  Future<GuestSessionModel?> getGuestSession() async {
    var baseUrl = FlavorConfig.instance.variables["baseUrl"];
    Uri _getSession = Uri.parse(baseUrl).replace(
        queryParameters: {"api_key": HttpService.apiKey, "language": "en-US"},
        pathSegments: ['3', 'authentication', 'guest_session', 'new']);
    logger.wtf('ini adalah baseUrl $_getSession');
    final response = await http.get(_getSession, headers: HttpService.headers);
    if (response.statusCode == 200) {
      logger.wtf(response.statusCode);
      var jsonString = response.body;
      logger.wtf(jsonDecode(jsonString));
      return guestSessionModelFromJson(jsonString);
    }
    return null;
  }

}
