import 'dart:convert';
import 'package:flutter_flavor/flutter_flavor.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import '../../../data/http_service.dart';
import '../../../model/up_coming_model.dart';
import '../../../utils/utils.dart';

class UpComingProvider extends GetConnect {

  Future<UpComingModel?> getUpComing({String? page}) async {
    var baseUrl = FlavorConfig.instance.variables["baseUrl"];
    Uri _getUpcoming = Uri.parse(baseUrl).replace(
        queryParameters: {"api_key": HttpService.apiKey, "language": "en-US", "page": '$page'},
        pathSegments: ['3', 'movie', 'upcoming']);
    logger.wtf('ini adalah baseUrl $_getUpcoming');
    final response = await http.get(_getUpcoming, headers: HttpService.headers);
    if (response.statusCode == 200) {
      logger.wtf(response.statusCode);
      var jsonString = response.body;
      logger.wtf(jsonDecode(jsonString));
      return upComingModelFromJson(jsonString);
    }
    return null;
  }

}
