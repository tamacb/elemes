import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';

import '../../../utils/utils.dart';
import '../../detail/controllers/detail_controller.dart';
import '../../detail/providers/detail_movie_provider.dart';
import '../../detail/views/detail_view.dart';
import '../controllers/upcoming_controller.dart';

class UpcomingView extends GetView<UpcomingController> {
  final DetailController _detailController = Get.put(DetailController(detailMovieProvider: DetailMovieProvider()));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Upcoming',
          style: GoogleFonts.nunitoSans(
            color: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.w600,
          ),
        ),
        flexibleSpace: const Image(
          image: AssetImage('assets/bg.jpg'),
          fit: BoxFit.cover,
        ),
        centerTitle: true,
        leading: const SizedBox(height: 1),
      ),
      body: Obx(() => LazyLoadScrollView(
            onEndOfPage: () => controller.loadMoreUpComing(),
            isLoading: controller.isLoadingUpComing.value,
            child: ListView(
              children: [
                AlignedGridView.count(
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  crossAxisCount: 3,
                  mainAxisSpacing: 4,
                  crossAxisSpacing: 4,
                  itemCount: controller.resultUpComing.length,
                  itemBuilder: (context, index) => Card(
                      child: GestureDetector(
                    onTap: () {
                      _detailController
                          .getDetail(id: controller.resultUpComing[index].id.toString())
                          .whenComplete(() => Get.to(DetailView()));
                    },
                    child: Container(
                      height: Get.height * 0.28,
                      color: Colors.white,
                      child: Column(
                        children: [
                          (controller.resultUpComing[index].posterPath == null)
                              ? const Text('Image not available')
                              : ClipRRect(
                                  borderRadius: BorderRadius.circular(5.0),
                                  child: SizedBox(
                                    height: Get.height * 0.22,
                                    child: Image.network(
                                      'https://image.tmdb.org/t/p/w500/${controller.resultUpComing[index].posterPath}',
                                    ),
                                  ),
                                ),
                          Padding(
                            padding: const EdgeInsets.only(left: 4.0),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                controller.resultUpComing[index].title!,
                                style: GoogleFonts.openSans(
                                  color: Colors.black,
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold,
                                ),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 4.0),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                dateFormat(date: controller.resultUpComing[index].releaseDate).toString().substring(0, 10),
                                style: GoogleFonts.openSans(
                                  color: Colors.black,
                                  fontSize: 12,
                                  fontWeight: FontWeight.normal,
                                ),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  )),
                ),
                if (controller.isLoadingUpComing.value)
                  const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Center(
                        child: CircularProgressIndicator(
                      semanticsLabel: 'loading data',
                    )),
                  )
              ],
            ),
          )),
    );
  }
}
