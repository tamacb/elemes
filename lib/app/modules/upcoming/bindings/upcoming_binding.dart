import 'package:elemes_app/app/modules/upcoming/providers/up_coming_provider.dart';
import 'package:get/get.dart';

import '../controllers/upcoming_controller.dart';

class UpcomingBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<UpcomingController>(
      () => UpcomingController(upcomingProvider: UpComingProvider()),
    );
  }
}
