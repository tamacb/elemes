import 'package:get/get.dart';

import '../../../model/up_coming_model.dart';
import '../../../utils/utils.dart';
import '../providers/up_coming_provider.dart';

class UpcomingController extends GetxController {
  UpComingProvider upcomingProvider;

  UpcomingController({required this.upcomingProvider});

  final page = 1.obs;
  late RxInt maxPage = 0.obs;

  final isLoadingUpComing = false.obs;

  @override
  void onInit() {
    super.onInit();
    getAllUpcoming();
  }

  Future<int> incrementPage() async => page.value++;

  final resultUpComing = <ResultUpComing>[].obs;

  Future getAllUpcoming() async {
    try {
      isLoadingUpComing.value = true;
      final res = await upcomingProvider.getUpComing(page: page.value.toString());
      // logger.wtf(res!.data!.data!.toList());
      resultUpComing.assignAll(res!.results!);
      maxPage.value = res.totalPages!;
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingUpComing.value = false;
    }
  }

  Future loadMoreUpComing() async {
    if (page.value != maxPage.value){
      await incrementPage();
      await getMoreUpComing();
    }
  }

  Future getMoreUpComing() async {
    try {
      isLoadingUpComing.value = true;
      final res = await upcomingProvider.getUpComing(page: page.value.toString());
      // logger.wtf(res!.data!.data!.toList());
      resultUpComing.addAll(res!.results!);
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingUpComing.value = false;
    }
  }
}
