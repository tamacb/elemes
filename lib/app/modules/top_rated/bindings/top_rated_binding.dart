import 'package:elemes_app/app/modules/top_rated/providers/top_rated_provider.dart';
import 'package:get/get.dart';

import '../controllers/top_rated_controller.dart';

class TopRatedBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<TopRatedController>(
      () => TopRatedController(topRatedProvider: TopRatedProvider()),
    );
  }
}
