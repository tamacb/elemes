import 'package:get/get.dart';

import '../../../model/top_rated_model.dart';
import '../../../utils/utils.dart';
import '../providers/top_rated_provider.dart';

class TopRatedController extends GetxController {
  TopRatedProvider topRatedProvider;

  TopRatedController({required this.topRatedProvider});

  final page = 1.obs;

  @override
  void onInit() {
    super.onInit();
    getAllTopRated();
  }

  final isLoadingToDetail = false.obs;
  final isLoadingTopRated = false.obs;
  final isLoadingTopRatedLoadMore = false.obs;

  final resultTopRated = <ResultTopRated>[].obs;

  Future getAllTopRated() async {
    try {
      isLoadingTopRated.value = true;
      final res = await topRatedProvider.getTopRated(page: page.value.toString());
      // logger.wtf(res!.data!.data!.toList());
      resultTopRated.assignAll(res!.results!);
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingTopRated.value = false;
    }
  }

  Future getMoreTopRated() async {
    try {
      isLoadingTopRatedLoadMore.value = true;
      final res = await topRatedProvider.getTopRated(page: page.value.toString());
      // logger.wtf(res!.data!.data!.toList());
      resultTopRated.addAll(res!.results!);
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingTopRatedLoadMore.value = false;
    }
  }

  Future<int> incrementPage() async => page.value++;

  Future loadMoreTopRated() async {
    await incrementPage();
    await getMoreTopRated();
  }
}
