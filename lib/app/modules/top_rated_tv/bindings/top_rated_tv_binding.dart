import 'package:elemes_app/app/modules/top_rated_tv/providers/top_rated_tv_provider.dart';
import 'package:get/get.dart';

import '../controllers/top_rated_tv_controller.dart';

class TopRatedTvBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<TopRatedTvController>(
      () => TopRatedTvController(topRatedTvProvider: TopRatedTvProvider()),
    );
  }
}
