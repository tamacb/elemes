import 'package:get/get.dart';

import '../../../model/tv_model.dart';
import '../../../utils/utils.dart';
import '../providers/top_rated_tv_provider.dart';

class TopRatedTvController extends GetxController {
  TopRatedTvProvider topRatedTvProvider;

  TopRatedTvController({required this.topRatedTvProvider});

  final page = 1.obs;
  late RxInt maxPage = 0.obs;

  final isLoadingTopRatedTv = false.obs;

  @override
  void onInit() {
    super.onInit();
    getAllTopRatedTv();
  }

  Future<int> incrementPage() async => page.value++;

  final resultTopRatedTv = <ResultTv>[].obs;

  Future getAllTopRatedTv() async {
    try {
      isLoadingTopRatedTv.value = true;
      final res = await topRatedTvProvider.getTopRatedTv(page: page.value.toString());
      // logger.wtf(res!.data!.data!.toList());
      resultTopRatedTv.assignAll(res!.results!);
      maxPage.value = res.totalPages!;
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingTopRatedTv.value = false;
    }
  }

  Future loadMoreTopRatedTv() async {
    if (page.value != maxPage.value){
      await incrementPage();
      await getMoreTopRatedTv();
    }
  }

  Future getMoreTopRatedTv() async {
    try {
      isLoadingTopRatedTv.value = true;
      final res = await topRatedTvProvider.getTopRatedTv(page: page.value.toString());
      // logger.wtf(res!.data!.data!.toList());
      resultTopRatedTv.addAll(res!.results!);
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingTopRatedTv.value = false;
    }
  }
}
