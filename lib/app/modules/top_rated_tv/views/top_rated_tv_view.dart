import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';

import '../../detail_tv/controllers/detail_tv_controller.dart';
import '../../detail_tv/providers/detail_tv_provider.dart';
import '../../detail_tv/views/detail_tv_view.dart';
import '../controllers/top_rated_tv_controller.dart';

class TopRatedTvView extends GetView<TopRatedTvController> {
  final DetailTvController _detailTvController = Get.put(DetailTvController(detailTvProvider: DetailTvProvider()));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Top Rated TV',
          style: GoogleFonts.nunitoSans(
            color: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.w600,
          ),
        ),
        flexibleSpace: const Image(
          image: AssetImage('assets/bg.jpg'),
          fit: BoxFit.cover,
        ),
        centerTitle: true,
        leading: const SizedBox(height: 1),
      ),
      body: Obx(() => LazyLoadScrollView(
        onEndOfPage: () => controller.loadMoreTopRatedTv(),
        isLoading: controller.isLoadingTopRatedTv.value,
        child: ListView(
          children: [
            AlignedGridView.count(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              crossAxisCount: 3,
              mainAxisSpacing: 4,
              crossAxisSpacing: 4,
              itemCount: controller.resultTopRatedTv.length,
              itemBuilder: (context, index) => Card(
                  child: GestureDetector(
                    onTap: () {
                      _detailTvController
                          .getDetailTv(id: controller.resultTopRatedTv[index].id.toString())
                          .whenComplete(() => Get.to(DetailTvView()));
                    },
                    child: Container(
                      height: Get.height * 0.28,
                      color: Colors.white,
                      child: Column(
                        children: [
                          (controller.resultTopRatedTv[index].posterPath == null)
                              ? const Text('Image not available')
                              : ClipRRect(
                            borderRadius: BorderRadius.circular(5.0),
                            child: SizedBox(
                              height: Get.height * 0.22,
                              child: Image.network(
                                'https://image.tmdb.org/t/p/w500/${controller.resultTopRatedTv[index].posterPath}',
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 4.0),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                controller.resultTopRatedTv[index].originalName!,
                                style: GoogleFonts.openSans(
                                  color: Colors.black,
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold,
                                ),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 4.0),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text('Popularity ${controller.resultTopRatedTv[index].popularity}',
                                style: GoogleFonts.openSans(
                                  color: Colors.black,
                                  fontSize: 12,
                                  fontWeight: FontWeight.normal,
                                ),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  )),
            ),
            if (controller.isLoadingTopRatedTv.value)
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: Center(
                    child: CircularProgressIndicator(
                      semanticsLabel: 'loading data',
                    )),
              )
          ],
        ),
      )),
    );
  }
}
