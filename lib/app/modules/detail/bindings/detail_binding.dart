import 'package:elemes_app/app/modules/detail/providers/detail_movie_provider.dart';
import 'package:elemes_app/app/modules/home/providers/home_provider.dart';
import 'package:get/get.dart';

import '../controllers/detail_controller.dart';

class DetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailController>(
      () => DetailController(detailMovieProvider: DetailMovieProvider()),
    );
  }
}
