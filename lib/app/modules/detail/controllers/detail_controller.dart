import 'package:elemes_app/app/model/common_message_model.dart';
import 'package:elemes_app/app/modules/detail/providers/detail_movie_provider.dart';
import 'package:elemes_app/app/modules/session/controllers/session_controller.dart';
import 'package:elemes_app/app/modules/session/providers/session_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../../data/const.dart';
import '../../../model/detail_model.dart';
import '../../../utils/utils.dart';

class DetailController extends GetxController {
  DetailMovieProvider detailMovieProvider;
  DetailController({required this.detailMovieProvider});

  final SessionController _sessionController = Get.put(SessionController(sessionProvider: SessionProvider()));

  final count = 0.obs;
  final movieId = "".obs;
  final isLoadingDetailMovie = false.obs;
  final isLoadingRateMovie = false.obs;

  var boxSession = GetStorage();

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  final ratingEditingController = TextEditingController();

  final resultDetail = <DetailModel>[].obs;

  Future getDetail({String? id}) async {
    try {
      isLoadingDetailMovie.value = true;
      final res = await detailMovieProvider.getDetailMovie(id: '$id');
      // logger.wtf(res!.data!.data!.toList());
      resultDetail.assign(res!);
      movieId.value = res.id.toString();
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingDetailMovie.value = false;
    }
  }

  final resultRateMovie = <CommonMessageModel>[].obs;

  Future rateMovie() async {
    try {
      isLoadingRateMovie.value = true;
      final res = await detailMovieProvider.rateMovie(
              body: {"value": ratingEditingController.text}, idMovie: movieId.value, sessionId: boxSession.read(guestSessionId));
      if (res!.success == false) {
            Get.snackbar('${res.statusMessage}', 'Yapp invalid value or token expired, just try again', backgroundColor: Colors.white);
            _sessionController.getGuestSession();
          } else {
            Get.snackbar('${res.statusMessage}', 'your rating saved', backgroundColor: Colors.white);
          }
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingRateMovie.value = false;
      ratingEditingController.clear();
    }
  }
}
