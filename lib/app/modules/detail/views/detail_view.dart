import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../widgets/inputTextFormFieldBase.dart';
import '../controllers/detail_controller.dart';

class DetailView extends GetView<DetailController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Details Movie',
          style: GoogleFonts.nunitoSans(
            color: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.w600,
          ),
        ),
        flexibleSpace: const Image(
          image: AssetImage('assets/bg.jpg'),
          fit: BoxFit.cover,
        ),
        centerTitle: true,
        leading: const SizedBox(width: 1),
        actions: [
          GestureDetector(
              onTap: () {
                sheetButtonRating();
              },
              child: const Padding(
                padding: EdgeInsets.only(bottom: 8.0, top: 8.0, left: 8.0, right: 25.0),
                child: Icon(
                  Icons.star,
                  color: Colors.white,
                  size: 35,
                ),
              )),
        ],
      ),
      body: ListView.builder(
          shrinkWrap: true,
          itemCount: controller.resultDetail.length,
          itemBuilder: (context, index) => Container(
                color: Colors.white,
                child: Stack(
                  children: [
                    Column(
                      children: [
                        Container(
                          child: (controller.resultDetail[index].backdropPath == null)
                              ? Padding(
                                  padding: const EdgeInsets.only(top: 20.0),
                                  child: Text(
                                    'thumbnail not available',
                                    style: GoogleFonts.openSans(
                                      color: Colors.black,
                                      fontSize: 20,
                                      fontWeight: FontWeight.w300,
                                    ),
                                  ),
                                )
                              : Image.network('https://image.tmdb.org/t/p/w500/${controller.resultDetail[index].backdropPath}',
                                  fit: BoxFit.cover),
                        ),
                        Container(color: Colors.grey),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 50.0, right: 20.0, left: 20.0),
                      child: Column(
                        children: [
                          SizedBox(
                            height: 100.0,
                            child: Center(
                                child: Text(
                              '${controller.resultDetail[index].title}',
                              style: GoogleFonts.nunitoSans(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            )),
                          ),
                          SingleChildScrollView(
                            child: Column(
                              children: [
                                Container(
                                  decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(5.0)),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          'Trailers',
                                          style: GoogleFonts.nunitoSans(
                                            color: Colors.black,
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                      (controller.resultDetail[index].posterPath == null)
                                          ? Padding(
                                        padding: const EdgeInsets.only(top: 20.0),
                                        child: Text(
                                          'poster not available',
                                          style: GoogleFonts.openSans(
                                            color: Colors.black,
                                            fontSize: 20,
                                            fontWeight: FontWeight.w300,
                                          ),
                                        ),
                                      )
                                          : SizedBox(
                                        height: 200.0,
                                        child: Image.network(
                                            'https://image.tmdb.org/t/p/w500/${controller.resultDetail[index].posterPath}',
                                            fit: BoxFit.cover),
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                buildContent(index, nameOfContent: "Descriptions", desc: controller.resultDetail[index].overview),
                                buildContent(index,
                                    nameOfContent: "Popularity", desc: controller.resultDetail[index].popularity.toString()),
                                buildContent(index,
                                    nameOfContent: "Release", desc: controller.resultDetail[index].releaseDate.toString()),
                                buildContent(index,
                                    nameOfContent: "Runtime",
                                    desc: controller.resultDetail[index].runtime.toString(),
                                    additional: ' Min'),
                                buildContent(index,
                                    nameOfContent: "Revenue", desc: controller.resultDetail[index].revenue.toString()),
                                buildContent(index,
                                    nameOfContent: "Vote Count", desc: controller.resultDetail[index].voteCount.toString()),
                                buildContent(index,
                                    nameOfContent: "Vote Average", desc: controller.resultDetail[index].voteAverage.toString()),
                                buildContent(index, nameOfContent: "Status", desc: controller.resultDetail[index].status),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )),
    );
  }

  Column buildContent(
    int index, {
    String? nameOfContent,
    String? desc,
    String? additional = "",
  }) {
    return Column(
      children: [
        Container(
          decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(5.0)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 8.0, top: 8.0),
                child: Text(
                  nameOfContent!,
                  style: GoogleFonts.nunitoSans(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: RichText(
                  text: TextSpan(
                      style: GoogleFonts.nunitoSans(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.w300,
                      ),
                      children: <TextSpan>[
                        TextSpan(text: '$desc'),
                        TextSpan(text: '$additional'),
                      ]),
                ),
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 10,
        ),
      ],
    );
  }

  void sheetButtonRating() {
    Get.bottomSheet(Container(
      decoration: const BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.only(topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0))),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Wrap(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Rating movie',
                style: GoogleFonts.nunitoSans(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Text(
                'value min 0.5 and max 10',
                style: GoogleFonts.nunitoSans(
                  color: Colors.black,
                  fontSize: 14,
                  fontWeight: FontWeight.w300,
                ),
              ),
            ),
            InputTextFormFieldBase(
              textEditingController: controller.ratingEditingController,
              controller: controller,
              obscureText: false,
              hintText: 'rating..',
              textInputAction: TextInputAction.go,
              onSubmited: (val) {
                if (controller.ratingEditingController.text.isEmpty) {
                  Get.snackbar('field is empty', 'please fill first');
                } else {
                  controller.rateMovie();
                  Get.until((route) => Get.isBottomSheetOpen == false);
                }
              }, textInputType: TextInputType.number,
            ),
          ],
        ),
      ),
    ));
  }
}
