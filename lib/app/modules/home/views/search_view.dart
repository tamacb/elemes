import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';

import '../../../widgets/will_pop.dart';
import '../../detail/controllers/detail_controller.dart';
import '../../detail/providers/detail_movie_provider.dart';
import '../../detail/views/detail_view.dart';
import '../controllers/home_controller.dart';

class SearchView extends GetView<HomeController> {
  final DetailController _detailController = Get.put(DetailController(detailMovieProvider: DetailMovieProvider()));

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => willPopBackClearField(controller.keywordEditingController),
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            'Search Result',
            style: GoogleFonts.nunitoSans(
              color: Colors.white,
              fontSize: 20,
              fontWeight: FontWeight.w600,
            ),
          ),
          flexibleSpace: const Image(
            image: AssetImage('assets/bg.jpg'),
            fit: BoxFit.cover,
          ),
          centerTitle: true,
          leading: const SizedBox(height: 1),
        ),
        body: Obx(() => LazyLoadScrollView(
              onEndOfPage: () => controller.loadMoreSearch(),
              isLoading: controller.isLoadingSearch.value,
              child: (controller.resultSearch.isEmpty)
                  ? const Center(child: Text('data not found'))
                  : ListView(
                      children: [
                        AlignedGridView.count(
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          crossAxisCount: 3,
                          mainAxisSpacing: 4,
                          crossAxisSpacing: 4,
                          itemCount: controller.resultSearch.length,
                          itemBuilder: (context, index) => Card(
                              child: GestureDetector(
                            onTap: () {
                              _detailController
                                  .getDetail(id: controller.resultSearch[index].id.toString())
                                  .whenComplete(() => Get.to(DetailView()));
                            },
                            child: Container(
                              height: Get.height * 0.28,
                              color: Colors.white,
                              child: Column(
                                children: [
                                  (controller.resultSearch[index].posterPath == null)
                                      ? const Text('Image not available')
                                      : ClipRRect(
                                          borderRadius: BorderRadius.circular(5.0),
                                          child: SizedBox(
                                            height: Get.height * 0.2,
                                            child: Image.network(
                                              'https://image.tmdb.org/t/p/w500/${controller.resultSearch[index].posterPath}',
                                            ),
                                          ),
                                        ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 4.0),
                                    child: Align(
                                      alignment: Alignment.topLeft,
                                      child: Text(
                                        controller.resultSearch[index].title!,
                                        style: GoogleFonts.openSans(
                                          color: Colors.black,
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                        ),
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 4.0),
                                    child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        controller.resultSearch[index].releaseDate.toString(),
                                        style: GoogleFonts.openSans(
                                          color: Colors.black,
                                          fontSize: 12,
                                          fontWeight: FontWeight.normal,
                                        ),
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          )),
                        ),
                        if (controller.isLoadingSearch.value)
                          const Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Center(
                                child: CircularProgressIndicator(
                              semanticsLabel: 'loading data',
                            )),
                          )
                      ],
                    ),
            )),
      ),
    );
  }
}
