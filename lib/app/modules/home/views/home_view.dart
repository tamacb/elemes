import 'package:elemes_app/app/modules/detail_tv/views/detail_tv_view.dart';
import 'package:elemes_app/app/modules/home/views/search_view.dart';
import 'package:elemes_app/app/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../data/const.dart';
import '../../../routes/app_pages.dart';
import '../../../widgets/header_content.dart';
import '../../../widgets/inputTextFormFieldBase.dart';
import '../../detail/controllers/detail_controller.dart';
import '../../detail/providers/detail_movie_provider.dart';
import '../../detail/views/detail_view.dart';
import '../../detail_tv/controllers/detail_tv_controller.dart';
import '../../detail_tv/providers/detail_tv_provider.dart';
import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  final DetailController _detailController = Get.put(DetailController(detailMovieProvider: DetailMovieProvider()));
  final DetailTvController _detailTvController = Get.put(DetailTvController(detailTvProvider: DetailTvProvider()));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: controller.scaffoldKey,
      drawer: buildDrawer(),
      appBar: AppBar(
        flexibleSpace: const Image(
          image: AssetImage('assets/bg.jpg'),
          fit: BoxFit.cover,
        ),
        title: Text('EleMovies',
            style: GoogleFonts.openSans(
              color: Colors.white,
              fontSize: 20,
              fontWeight: FontWeight.w600,
            )),
        centerTitle: true,
        actions: [
          IconButton(
              onPressed: () {
                sheetButtonSearch();
              },
              icon: const Icon(
                Icons.search,
                color: Colors.deepPurple,
              ))
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            buildHeader(
                name: 'Now Playing',
                more: () {
                  Get.toNamed(Routes.NOW_PLAYING);
                }),
            Obx(() => SizedBox(
                  height: Get.height * 0.4,
                  child: (controller.isLoadingNowPlaying.value)
                      ? const Center(child: CircularProgressIndicator())
                      : ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemCount: controller.resultNowPlaying.take(10).length,
                          itemBuilder: (context, index) => buildMediumCard(index,
                                  releaseDate: dateFormat(date: controller.resultNowPlaying[index].releaseDate)
                                      .toString()
                                      .substring(0, 10), toDetail: () {
                                _detailController
                                    .getDetail(id: controller.resultNowPlaying[index].id.toString())
                                    .whenComplete(() => Get.to(DetailView()));
                              },
                                  posterPhoto: controller.resultNowPlaying[index].posterPath,
                                  title: controller.resultNowPlaying[index].title)),
                )),
            buildHeader(
                name: 'Upcoming',
                more: () {
                  Get.toNamed(Routes.UPCOMING);
                }),
            Obx(() => SizedBox(
                  height: Get.height * 0.3,
                  child: (controller.isLoadingUpComing.value)
                      ? const Center(child: CircularProgressIndicator())
                      : ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemCount: controller.resultUpcoming.take(10).length,
                          itemBuilder: (context, index) => buildSmallCard(index,
                                  posterPhoto: controller.resultUpcoming[index].posterPath,
                                  releaseDate:
                                      dateFormat(date: controller.resultUpcoming[index].releaseDate).toString().substring(0, 10),
                                  title: controller.resultUpcoming[index].title, toDetail: () {
                                _detailController
                                    .getDetail(id: controller.resultUpcoming[index].id.toString())
                                    .whenComplete(() => Get.to(DetailView()));
                              })),
                )),
            buildHeader(
                name: 'Popular Casting',
                more: () {
                  Get.toNamed(Routes.PEOPLE);
                }),
            Obx(() => SizedBox(
              height: Get.height * 0.17,
              child: (controller.isLoadingPeople.value)
                  ? const Center(child: CircularProgressIndicator())
                  : ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: controller.resultPeople.take(10).length,
                  itemBuilder: (context, index) => Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(50.0),
                            child: CircleAvatar(
                              backgroundColor: Colors.grey,
                                maxRadius: 45.0,
                                child: Image.network('https://image.tmdb.org/t/p/w500${controller.resultPeople[index].profilePath}', fit: BoxFit.contain,))),
                      ),
                      Text('${controller.resultPeople[index].name}', style: GoogleFonts.openSans(
                        color: Colors.black,
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                      ),overflow: TextOverflow.ellipsis,)
                    ],
                  )),
            )),

            buildHeader(
                name: 'Top Rated',
                more: () {
                  Get.toNamed(Routes.TOP_RATED);
                }),
            Obx(() => SizedBox(
                  height: Get.height * 0.4,
                  child: (controller.isLoadingPopularMovie.value)
                      ? const Center(child: CircularProgressIndicator())
                      : ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemCount: controller.resultPopularMovie.take(10).length,
                          itemBuilder: (context, index) => buildMediumCard(index,
                                  releaseDate: dateFormat(date: controller.resultPopularMovie[index].releaseDate)
                                      .toString()
                                      .substring(0, 10), toDetail: () {
                                _detailController
                                    .getDetail(id: controller.resultPopularMovie[index].id.toString())
                                    .whenComplete(() => Get.to(DetailView()));
                              },
                                  posterPhoto: controller.resultPopularMovie[index].posterPath,
                                  title: controller.resultPopularMovie[index].title)),
                )),
            buildHeader(
                name: 'Popular',
                more: () {
                  Get.toNamed(Routes.POPULAR);
                }),
            Obx(() => SizedBox(
                  height: Get.height * 0.4,
                  child: (controller.isLoadingTopRated.value)
                      ? const Center(child: CircularProgressIndicator())
                      : ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemCount: controller.resultTopRated.take(10).length,
                          itemBuilder: (context, index) => buildMediumCard(index,
                                  releaseDate: dateFormat(date: controller.resultTopRated[index].releaseDate)
                                      .toString()
                                      .substring(0, 10), toDetail: () {
                                _detailController
                                    .getDetail(id: controller.resultTopRated[index].id.toString())
                                    .whenComplete(() => Get.to(DetailView()));
                              },
                                  posterPhoto: controller.resultTopRated[index].posterPath,
                                  title: controller.resultTopRated[index].title)),
                )),
            Padding(
              padding: const EdgeInsets.only(top: 20, bottom: 10),
              child: Container(color: Colors.grey, height: 10),
            ),
            buildHeader(
                name: 'Popular TV',
                more: () {
                  Get.toNamed(Routes.POPULAR_TV);
                }),
            Obx(() => SizedBox(
                  height: Get.height * 0.3,
                  child: (controller.isLoadingPopularTv.value)
                      ? const Center(child: CircularProgressIndicator())
                      : ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemCount: controller.resultPopularTv.take(10).length,
                          itemBuilder: (context, index) => buildSmallCard(index,
                                  posterPhoto: controller.resultPopularTv[index].posterPath,
                                  releaseDate: controller.resultPopularTv[index].originalName,
                                  title: controller.resultPopularTv[index].name, toDetail: () {
                                _detailTvController
                                    .getDetailTv(id: controller.resultPopularTv[index].id.toString())
                                    .whenComplete(() => Get.to(DetailTvView()));
                              })),
                )),
          ],
        ),
      ),
    );
  }

  Drawer buildDrawer() {
    return Drawer(
      child: ListView(
        children: [
          UserAccountsDrawerHeader(
            decoration: const BoxDecoration(image: DecorationImage(fit: BoxFit.cover, image: AssetImage('assets/bg.jpg'))),
            accountName: const Text('EleMovie'),
            accountEmail: const Text('elemes@movie.test'),
            currentAccountPicture: SvgPicture.network(
                'https://www.themoviedb.org/assets/2/v4/logos/v2/blue_short-8e7b30f73a4020692ccca9c88bafe5dcb6f8a62a4c6bc55cd9ba82bb2cd95f6c.svg',
                color: Colors.white),
            arrowColor: Colors.teal,
            otherAccountsPicturesSize: const Size(30.0, 30.0),
          ),
          ListTile(
              leading: const Icon(Icons.home),
              title: const Text('Home'),
              onTap: () {
                controller.closeDrawer();
              }),
          ListTile(
              leading: const Icon(Icons.play_circle_fill),
              title: const Text('Popular Movies'),
              onTap: () {
                Get.toNamed(Routes.POPULAR);
                controller.closeDrawer();
              }),
          ListTile(
              leading: const Icon(Icons.play_circle_fill),
              title: const Text('Now Playing'),
              onTap: () {
                Get.toNamed(Routes.NOW_PLAYING);
                controller.closeDrawer();
              }),
          ListTile(
              leading: const Icon(Icons.trending_up_outlined),
              title: const Text('Top Rated'),
              onTap: () {
                Get.toNamed(Routes.TOP_RATED);
                controller.closeDrawer();
              }),
          ListTile(
              leading: const Icon(Icons.play_circle_outline),
              title: const Text('Upcoming'),
              onTap: () {
                Get.toNamed(Routes.UPCOMING);
                controller.closeDrawer();
              }),
          buildDivider(),
          ListTile(
              leading: const Icon(Icons.tv),
              title: const Text('Popular TV'),
              onTap: () {
                Get.toNamed(Routes.POPULAR_TV);
                controller.closeDrawer();
              }),
          ListTile(
              leading: const Icon(Icons.tv),
              title: const Text('Top Rated TV'),
              onTap: () {
                Get.toNamed(Routes.TOP_RATED_TV);
                controller.closeDrawer();
              }),
          ListTile(
              leading: const Icon(Icons.live_tv_outlined),
              title: const Text('On Air TV'),
              onTap: () {
                Get.toNamed(Routes.ON_AIR_TV);
                controller.closeDrawer();
              }),
          ListTile(
              leading: const Icon(Icons.live_tv),
              title: const Text('Airing Today TV'),
              onTap: () {
                Get.toNamed(Routes.ON_TODAY_TV);
                controller.closeDrawer();
              }),
          buildDivider(),
          ListTile(
              leading: const Icon(Icons.people_outline),
              title: const Text('Popular Casting'),
              onTap: () {
                Get.toNamed(Routes.PEOPLE);
                controller.closeDrawer();
              }),
        ],
      ),
    );
  }

  Padding buildDivider() {
    return Padding(
      padding: const EdgeInsets.only(left: 8, right: 8),
      child: Container(color: Colors.grey, height: 1),
    );
  }

  void sheetButtonSearch() {
    Get.bottomSheet(Container(
      decoration: const BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.only(topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0))),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: InputTextFormFieldBase(
          textEditingController: controller.keywordEditingController,
          controller: controller,
          obscureText: false,
          hintText: 'search movie..',
          textInputAction: TextInputAction.search,
          onSubmited: (val) {
            if (controller.keywordEditingController.text.isEmpty) {
              Get.snackbar('field is empty', 'please fill first');
            } else {
              controller.getSearch().then((value) => Get.to(SearchView()));
              Get.until((route) => Get.isBottomSheetOpen == false);
            }
          },
          textInputType: TextInputType.text,
        ),
      ),
    ));
  }

  Card buildMediumCard(int index, {VoidCallback? toDetail, String? posterPhoto, String? title, String? releaseDate}) {
    return Card(
        child: GestureDetector(
      onTap: toDetail,
      child: Container(
        color: Colors.white,
        width: Get.width * 0.5,
        child: Column(
          children: [
            Expanded(
              flex: 10,
              child: Image.network('https://image.tmdb.org/t/p/w500$posterPhoto'),
            ),
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    title!,
                    style: GoogleFonts.openSans(
                      color: Colors.black,
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    releaseDate!,
                    style: GoogleFonts.openSans(
                      color: Colors.black,
                      fontSize: 12,
                      fontWeight: FontWeight.normal,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    ));
  }

  Card buildSmallCard(int index, {VoidCallback? toDetail, String? posterPhoto, String? title, String? releaseDate}) {
    return Card(
        child: GestureDetector(
      onTap: toDetail,
      child: Container(
        color: Colors.white,
        width: Get.width * 0.3,
        child: Column(
          children: [
            Expanded(
              flex: 10,
              child: Image.network('https://image.tmdb.org/t/p/w500$posterPhoto'),
            ),
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.only(left: 4.0),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    title!,
                    style: GoogleFonts.openSans(
                      color: Colors.black,
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.only(left: 4.0),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    releaseDate!,
                    style: GoogleFonts.openSans(
                      color: Colors.black,
                      fontSize: 12,
                      fontWeight: FontWeight.normal,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    ));
  }
}
