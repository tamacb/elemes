import 'package:elemes_app/app/model/guest_session_model.dart';
import 'package:elemes_app/app/model/up_coming_model.dart';
import 'package:elemes_app/app/modules/people/providers/people_provider.dart';
import 'package:elemes_app/app/modules/popular/providers/popular_movie_provider.dart';
import 'package:elemes_app/app/modules/popular_tv/providers/popular_tv_provider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../model/now_playing_model.dart';
import '../../../model/people_model.dart';
import '../../../model/popular_movie_model.dart';
import '../../../model/search_model.dart';
import '../../../model/top_rated_model.dart';
import '../../../model/tv_model.dart';
import '../../../utils/utils.dart';
import '../../now_playing/providers/now_playing_provider.dart';
import '../../top_rated/providers/top_rated_provider.dart';
import '../../upcoming/providers/up_coming_provider.dart';
import '../providers/home_provider.dart';

class HomeController extends GetxController {
  NowPlayingProvider nowPlayingProvider;
  UpComingProvider upcomingProvider;
  TopRatedProvider topRatedProvider;
  HomeProvider homeProvider;
  PopularMovieProvider popularMovieProvider;
  PopularTvProvider popularTvProvider;
  PeopleProvider peopleProvider;

  HomeController(
      {required this.peopleProvider, required this.nowPlayingProvider,
      required this.homeProvider,
      required this.upcomingProvider,
      required this.topRatedProvider, required this.popularMovieProvider, required this.popularTvProvider});

  var scaffoldKey = GlobalKey<ScaffoldState>();

  void openDrawer() {
    scaffoldKey.currentState?.openDrawer();
  }

  void closeDrawer() {
    scaffoldKey.currentState?.openEndDrawer();
  }

  final page = 1.obs;
  final maxPageSearch = 0.obs;

  final isLoadingNowPlaying = false.obs;
  final isLoadingUpComing = false.obs;
  final isLoadingTopRated = false.obs;
  final isLoadingPopularMovie = false.obs;
  final isLoadingSearch = false.obs;
  final isLoadingPopularTv = false.obs;
  final isLoadingPeople = false.obs;

  final keywordEditingController = TextEditingController();

  @override
  void onInit() {
    super.onInit();
    getAllNowPlaying();
    getAllTopRated();
    getAllUpcoming();
    getAllPopular();
    getAllPopularTv();
    getPeople();
  }

  final resultPopularMovie = <ResultPopularMovie>[].obs;

  Future getAllPopular() async {
    try {
      isLoadingPopularMovie.value = true;
      final res = await popularMovieProvider.getPopularMovie(page: page.value.toString());
      // logger.wtf(res!.data!.data!.toList());
      resultPopularMovie.assignAll(res!.results!);
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingPopularMovie.value = false;
    }
  }

  final resultTopRated = <ResultTopRated>[].obs;

  Future getAllTopRated() async {
    try {
      isLoadingTopRated.value = true;
      final res = await topRatedProvider.getTopRated(page: page.value.toString());
      // logger.wtf(res!.data!.data!.toList());
      resultTopRated.assignAll(res!.results!);
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingTopRated.value = false;
    }
  }

  final resultUpcoming = <ResultUpComing>[].obs;

  Future getAllUpcoming() async {
    try {
      isLoadingUpComing.value = true;
      final res = await upcomingProvider.getUpComing(page: page.value.toString());
      // logger.wtf(res!.data!.data!.toList());
      resultUpcoming.assignAll(res!.results!);
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingUpComing.value = false;
    }
  }

  final resultNowPlaying = <ResultNowPlaying>[].obs;

  Future getAllNowPlaying() async {
    try {
      isLoadingNowPlaying.value = true;
      final res = await nowPlayingProvider.getNowPlaying(page: page.value.toString());
      // logger.wtf(res!.data!.data!.toList());
      resultNowPlaying.assignAll(res!.results!);
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingNowPlaying.value = false;
    }
  }

  final resultPopularTv = <ResultTv>[].obs;

  Future getAllPopularTv() async {
    try {
      isLoadingPopularTv.value = true;
      final res = await popularTvProvider.getPopularTv(page: page.value.toString());
      // logger.wtf(res!.data!.data!.toList());
      resultPopularTv.assignAll(res!.results!);
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingPopularTv.value = false;
    }
  }

  final resultPeople = <ResultPeople>[].obs;

  Future getPeople() async {
    try {
      isLoadingPeople.value = true;
      final res = await peopleProvider.getPeople(page: page.value.toString());
      resultPeople.assignAll(res!.results!);
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingPeople.value = false;
    }
  }

  final resultSearch = <ResultSearch>[].obs;

  Future getSearch() async {
    try {
      isLoadingSearch.value = true;
      final res = await homeProvider.getSearchMovie(page: page.value.toString(), query: keywordEditingController.text);
      // logger.wtf(res!.data!.data!.toList());
      resultSearch.assignAll(res!.results!);
      maxPageSearch.value = res.totalPages!;
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingSearch.value = false;
    }
  }

  Future<int> incrementPage() async => page.value++;

  Future loadMoreSearch() async {
    if (page.value != maxPageSearch.value) {
      await incrementPage();
      await getMoreSearch();
    }
  }

  Future getMoreSearch() async {
    try {
      isLoadingSearch.value = true;
      final res = await homeProvider.getSearchMovie(page: page.value.toString(), query: keywordEditingController.text);
      // logger.wtf(res!.data!.data!.toList());
      resultSearch.addAll(res!.results!);
      maxPageSearch.value = res.totalPages!;
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingSearch.value = false;
    }
  }
}
