import 'package:elemes_app/app/modules/home/providers/home_provider.dart';
import 'package:elemes_app/app/modules/now_playing/providers/now_playing_provider.dart';
import 'package:elemes_app/app/modules/people/providers/people_provider.dart';
import 'package:elemes_app/app/modules/popular/providers/popular_movie_provider.dart';
import 'package:elemes_app/app/modules/popular_tv/providers/popular_tv_provider.dart';
import 'package:elemes_app/app/modules/top_rated/providers/top_rated_provider.dart';
import 'package:elemes_app/app/modules/upcoming/providers/up_coming_provider.dart';
import 'package:get/get.dart';

import '../controllers/home_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(
      () => HomeController(
          nowPlayingProvider: NowPlayingProvider(),
          homeProvider: HomeProvider(),
          upcomingProvider: UpComingProvider(),
          topRatedProvider: TopRatedProvider(),
          popularMovieProvider: PopularMovieProvider(), popularTvProvider: PopularTvProvider(), peopleProvider: PeopleProvider()),
    );
  }
}
