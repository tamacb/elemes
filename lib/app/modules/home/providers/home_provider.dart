import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter_flavor/flutter_flavor.dart';
import 'package:get/get.dart';

import '../../../data/http_service.dart';
import '../../../model/guest_session_model.dart';
import '../../../model/search_model.dart';
import '../../../utils/utils.dart';

class HomeProvider extends GetConnect {
  Future<SearchModel?> getSearchMovie({String? page, String? query}) async {
    var baseUrl = FlavorConfig.instance.variables["baseUrl"];
    Uri _getSearch = Uri.parse(baseUrl).replace(queryParameters: {
      "api_key": HttpService.apiKey,
      "language": "en-US",
      "page": "$page",
      "query": "$query",
      "adult": "true"
    }, pathSegments: [
      '3',
      'search',
      'movie'
    ]);
    logger.wtf('ini adalah baseUrl $_getSearch');
    final response = await http.get(_getSearch, headers: HttpService.headers);
    if (response.statusCode == 200) {
      logger.wtf(response.statusCode);
      var jsonString = response.body;
      logger.wtf(jsonDecode(jsonString));
      return searchModelFromJson(jsonString);
    }
    return null;
  }
}
