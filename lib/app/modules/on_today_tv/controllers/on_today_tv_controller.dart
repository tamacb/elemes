import 'package:elemes_app/app/modules/on_today_tv/providers/on_today_tv_provider.dart';
import 'package:get/get.dart';

import '../../../model/tv_model.dart';
import '../../../utils/utils.dart';

class OnTodayTvController extends GetxController {
  OnTodayTvProvider onTodayTvProvider;

  OnTodayTvController({required this.onTodayTvProvider});

  final page = 1.obs;
  late RxInt maxPage = 0.obs;

  final isLoadingOnTodayTv = false.obs;

  @override
  void onInit() {
    super.onInit();
    getOnTodayTv();
  }

  Future<int> incrementPage() async => page.value++;

  final resultOnTodayTv = <ResultTv>[].obs;

  Future getOnTodayTv() async {
    try {
      isLoadingOnTodayTv.value = true;
      final res = await onTodayTvProvider.getOnTodayTv(page: page.value.toString());
      // logger.wtf(res!.data!.data!.toList());
      resultOnTodayTv.assignAll(res!.results!);
      maxPage.value = res.totalPages!;
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingOnTodayTv.value = false;
    }
  }

  Future loadMoreOnTodayTv() async {
    if (page.value != maxPage.value){
      await incrementPage();
      await getMoreOnTodayTv();
    }
  }

  Future getMoreOnTodayTv() async {
    try {
      isLoadingOnTodayTv.value = true;
      final res = await onTodayTvProvider.getOnTodayTv(page: page.value.toString());
      // logger.wtf(res!.data!.data!.toList());
      resultOnTodayTv.addAll(res!.results!);
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingOnTodayTv.value = false;
    }
  }
}
