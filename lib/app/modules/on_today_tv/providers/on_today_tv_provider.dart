import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter_flavor/flutter_flavor.dart';
import 'package:get/get.dart';

import '../../../data/http_service.dart';
import '../../../model/tv_model.dart';
import '../../../utils/utils.dart';

class OnTodayTvProvider extends GetConnect {
  Future<TvModel?> getOnTodayTv({String? page}) async {
    var baseUrl = FlavorConfig.instance.variables["baseUrl"];
    Uri _getOnTodayTv = Uri.parse(baseUrl).replace(
        queryParameters: {"api_key": HttpService.apiKey, "language": "en-US", "page": '$page'},
        pathSegments: ['3', 'tv', 'airing_today']);
    logger.wtf('ini adalah baseUrl $_getOnTodayTv');
    final response = await http.get(_getOnTodayTv, headers: HttpService.headers);
    if (response.statusCode == 200) {
      logger.wtf(response.statusCode);
      var jsonString = response.body;
      logger.wtf(jsonDecode(jsonString));
      return tvModelFromJson(jsonString);
    }
    return null;
  }
}
