import 'package:elemes_app/app/modules/detail_tv/controllers/detail_tv_controller.dart';
import 'package:elemes_app/app/modules/detail_tv/providers/detail_tv_provider.dart';
import 'package:elemes_app/app/modules/detail_tv/views/detail_tv_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';

import '../controllers/on_today_tv_controller.dart';

class OnTodayTvView extends GetView<OnTodayTvController> {
  final DetailTvController _detailTvController = Get.put(DetailTvController(detailTvProvider: DetailTvProvider()));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'On Today TV',
          style: GoogleFonts.nunitoSans(
            color: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.w600,
          ),
        ),
        flexibleSpace: const Image(
          image: AssetImage('assets/bg.jpg'),
          fit: BoxFit.cover,
        ),
        centerTitle: true,
        leading: const SizedBox(height: 1),
      ),
      body: Obx(() => LazyLoadScrollView(
        onEndOfPage: () => controller.loadMoreOnTodayTv(),
        isLoading: controller.isLoadingOnTodayTv.value,
        child: ListView(
          children: [
            AlignedGridView.count(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              crossAxisCount: 3,
              mainAxisSpacing: 4,
              crossAxisSpacing: 4,
              itemCount: controller.resultOnTodayTv.length,
              itemBuilder: (context, index) => Card(
                  child: GestureDetector(
                    onTap: () {
                      _detailTvController
                          .getDetailTv(id: controller.resultOnTodayTv[index].id.toString())
                          .whenComplete(() => Get.to(DetailTvView()));
                    },
                    child: Container(
                      height: Get.height * 0.28,
                      color: Colors.white,
                      child: Column(
                        children: [
                          (controller.resultOnTodayTv[index].posterPath == null)
                              ? const Text('Image not available')
                              : ClipRRect(
                            borderRadius: BorderRadius.circular(5.0),
                            child: SizedBox(
                              height: Get.height * 0.22,
                              child: Image.network(
                                'https://image.tmdb.org/t/p/w500/${controller.resultOnTodayTv[index].posterPath}',
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 4.0),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                controller.resultOnTodayTv[index].originalName!,
                                style: GoogleFonts.openSans(
                                  color: Colors.black,
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold,
                                ),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 4.0),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text('Popularity ${controller.resultOnTodayTv[index].popularity}',
                                style: GoogleFonts.openSans(
                                  color: Colors.black,
                                  fontSize: 12,
                                  fontWeight: FontWeight.normal,
                                ),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  )),
            ),
            if (controller.isLoadingOnTodayTv.value)
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: Center(
                    child: CircularProgressIndicator(
                      semanticsLabel: 'loading data',
                    )),
              )
          ],
        ),
      )),
    );
  }
}
