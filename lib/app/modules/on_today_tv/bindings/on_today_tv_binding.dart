import 'package:elemes_app/app/modules/on_today_tv/providers/on_today_tv_provider.dart';
import 'package:get/get.dart';

import '../controllers/on_today_tv_controller.dart';

class OnTodayTvBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OnTodayTvController>(
      () => OnTodayTvController(onTodayTvProvider: OnTodayTvProvider()),
    );
  }
}
