import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../controllers/splash_controller.dart';

class SplashView extends GetView<SplashController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<SplashController>(
          builder: (controller){
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Center(
                    child: Icon(
                      Icons.movie_outlined,
                      size: 45,
                    )),
                Text(
                  'EleMovie',
                  style: GoogleFonts.poppins(color: Colors.purple, fontSize: 40, fontWeight: FontWeight.w600),
                ),
              ],
            );
          },
          init: SplashController()),
    );
  }
}
