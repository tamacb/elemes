import 'package:elemes_app/app/modules/on_air_tv/providers/on_air_tv_provider.dart';
import 'package:get/get.dart';

import '../controllers/on_air_tv_controller.dart';

class OnAirTvBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OnAirTvController>(
      () => OnAirTvController(onAirTvProvider: OnAirTvProvider()),
    );
  }
}
