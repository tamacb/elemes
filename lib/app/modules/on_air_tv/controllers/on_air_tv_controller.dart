import 'package:elemes_app/app/modules/on_air_tv/providers/on_air_tv_provider.dart';
import 'package:get/get.dart';

import '../../../model/tv_model.dart';
import '../../../utils/utils.dart';

class OnAirTvController extends GetxController {
  OnAirTvProvider onAirTvProvider;

  OnAirTvController({required this.onAirTvProvider});

  final page = 1.obs;
  late RxInt maxPage = 0.obs;

  final isLoadingOnAirTv = false.obs;

  @override
  void onInit() {
    super.onInit();
    getOnAirTv();
  }

  Future<int> incrementPage() async => page.value++;

  final resultOnAirTv = <ResultTv>[].obs;

  Future getOnAirTv() async {
    try {
      isLoadingOnAirTv.value = true;
      final res = await onAirTvProvider.getOnAirTv(page: page.value.toString());
      // logger.wtf(res!.data!.data!.toList());
      resultOnAirTv.assignAll(res!.results!);
      maxPage.value = res.totalPages!;
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingOnAirTv.value = false;
    }
  }

  Future loadMoreOnAirTv() async {
    if (page.value != maxPage.value){
      await incrementPage();
      await getMoreOnAirTv();
    }
  }

  Future getMoreOnAirTv() async {
    try {
      isLoadingOnAirTv.value = true;
      final res = await onAirTvProvider.getOnAirTv(page: page.value.toString());
      // logger.wtf(res!.data!.data!.toList());
      resultOnAirTv.addAll(res!.results!);
    } catch (e) {
      logger.e(e);
    } finally {
      isLoadingOnAirTv.value = false;
    }
  }
}
