import 'dart:convert';

CommonMessageModel commonMessageModelFromJson(String str) => CommonMessageModel.fromJson(json.decode(str));

class CommonMessageModel {
  CommonMessageModel({
    this.success,
    this.statusCode,
    this.statusMessage,
  });

  final bool? success;
  final int? statusCode;
  final String? statusMessage;

  factory CommonMessageModel.fromJson(Map<String, dynamic> json) => CommonMessageModel(
    success: json["success"] == null ? null : json["success"],
    statusCode: json["status_code"] == null ? null : json["status_code"],
    statusMessage: json["status_message"] == null ? null : json["status_message"],
  );
}
