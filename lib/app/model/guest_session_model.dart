import 'dart:convert';

GuestSessionModel guestSessionModelFromJson(String str) => GuestSessionModel.fromJson(json.decode(str));

class GuestSessionModel {
  GuestSessionModel({
    this.success,
    this.guestSessionId,
    this.expiresAt,
  });

  final bool? success;
  final String? guestSessionId;
  final String? expiresAt;

  factory GuestSessionModel.fromJson(Map<String, dynamic> json) => GuestSessionModel(
    success: json["success"] == null ? null : json["success"],
    guestSessionId: json["guest_session_id"] == null ? null : json["guest_session_id"],
    expiresAt: json["expires_at"] == null ? null : json["expires_at"],
  );
}
