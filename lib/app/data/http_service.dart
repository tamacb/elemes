import 'dart:io';

class HttpService {

  static const apiKey = '131ba6b9d023b53099f7cf48af3176e0';
  final imageUrl = 'https://image.tmdb.org/t/p/w500/';

  static const Map<String, String> headers = {
    HttpHeaders.contentTypeHeader: 'application/json',
    HttpHeaders.acceptHeader: 'application/json',
  };
}


