import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class InputTextFormFieldBase extends StatelessWidget {
  const InputTextFormFieldBase(
      {Key? key,
      required this.textEditingController,
      required this.controller,
      required this.obscureText,
      this.hintText,
      this.label,
      this.suffix,
      this.textInputFormatter,
      this.returnValidation,
      this.validator, this.textInputAction, this.onSubmited, required this.textInputType})
      : super(key: key);

  final TextEditingController textEditingController;
  final GetxController controller;
  final bool obscureText;
  final String? hintText;
  final Widget? suffix;
  final Widget? label;
  final List<TextInputFormatter>? textInputFormatter;
  final dynamic returnValidation;
  final Function? validator;
  final TextInputAction? textInputAction;
  final dynamic onSubmited;
  final TextInputType textInputType;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: textInputType,
      textInputAction: textInputAction,
      onFieldSubmitted: onSubmited,
      cursorColor: Colors.black,
      key: key,
      validator: returnValidation,
      inputFormatters: textInputFormatter,
      controller: textEditingController,
      obscureText: obscureText,
      decoration: InputDecoration(
        label: label,
        labelStyle: GoogleFonts.nunitoSans(
          color: Colors.black,
          fontSize: 18,
          fontWeight: FontWeight.bold,
        ),
        hintText: hintText,
        hintStyle: GoogleFonts.nunitoSans(
          color: Colors.grey,
          fontSize: 18,
          fontWeight: FontWeight.bold,
        ), focusedBorder: const UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.black),
      ),));
  }
}
