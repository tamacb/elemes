
import 'package:flutter/cupertino.dart';

Future<bool> willPopBackClearField(TextEditingController textEditingController) async {
  // await showDialog or Show add banners or whatever
  // then
  textEditingController.clear();
  return true;
  // return true if the route to be popped
}