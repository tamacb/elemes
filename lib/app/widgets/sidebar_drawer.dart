import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SideBarDrawer extends StatelessWidget {
  String? name;
  String? email;
  SideBarDrawer({this.name, this.email, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          UserAccountsDrawerHeader(
            accountName: Text(name!),
            accountEmail: Text(email!),
            currentAccountPicture: SvgPicture.network(
                'https://www.themoviedb.org/assets/2/v4/logos/v2/blue_short-8e7b30f73a4020692ccca9c88bafe5dcb6f8a62a4c6bc55cd9ba82bb2cd95f6c.svg'),
            arrowColor: Colors.teal,
            otherAccountsPicturesSize: const Size(30.0, 30.0),
          ),
          ListTile(
              leading: const Icon(Icons.home),
              title: const Text('Home'),
              onTap: () {
              }),
          const ListTile(leading: Icon(Icons.play_circle_fill), title: Text('Now Playing')),
          const ListTile(leading: Icon(Icons.trending_up_outlined), title: Text('Top Rated')),
          const ListTile(leading: Icon(Icons.play_circle_outline), title: Text('Upcoming')),
        ],
      ),
    );
  }
}
