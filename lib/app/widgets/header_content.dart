import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Padding buildHeader({required String name, VoidCallback? more}) {
  return Padding(
    padding: const EdgeInsets.only(top: 8.0, left: 10.0, right: 10.0, bottom: 4),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          name,
          style: GoogleFonts.openSans(
            color: Colors.black,
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
        GestureDetector(
          onTap: more,
          child: Text(
            'more',
            style: GoogleFonts.nunitoSans(
              color: Colors.grey,
              fontSize: 14,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ],
    ),
  );
}
