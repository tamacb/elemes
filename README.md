# Project Title

ELEMES APP TEST CASE

## Overview

This project for recruitment testing Flutter Developer at Elemes.id.

### Prerequisites

What things you need to run and how to release, open project with android studio or VScode, open file pubspec.yaml and run

```
flutter pub get
```

### Running App

wait a step by step series to get a dependencies

```
flutter run
```
Or at android studio, click main.dart and shift+f10 or click run at tab bar

# APK

if need download .apk file, you can download at this url Gdrive

 https://drive.google.com/file/d/1x071Mi9n1nmfFe9rK1Z2YsbbJuYI79kx/view?usp=sharing 

### Release App

if need for release

```
flutter build apk
```

